# GetSim

## Description

getsim is a simple tool for copying Cactus simulations from a remote
machine to a local machine.  It uses rsync, and you can configure
which files are included using an rsync filter rules file.

## Usage

    getsim [-d <remotedir>] [-t <localdir>] <machine> <simpattern>...

- remotedir: The name of the directory, absolute or relative to your
  home directory on the remote machine which contains the simulations.
  Defaults to 'simulations'.
- localdir: The name of the directory on your local machine in which
  to place the simulations.  Defaults to ~/Simulations.  Can also be
  specified by the content of a file 'getsim.destdir' in your current
  directory.
- machine: The name of the machine containing the simulation.  This is
  passed to ssh, so you can use a hostname, or a machine configured in
  your .ssh/config file.
- simpattern: The name of a simulation, or a shell pattern for the
  names of the simulations to transfer

## Filter rules

Add the file getsim.filter.rules (from the source repository) to the
directory from which you will run getsim (e.g your Cactus directory)
to provide fine-grained control over which files to copy.  Edit the
file to customise the files you want to include or exclude.

## Examples

    getsim -d /scratch/myname/simulations mycluster bbh_1_24

    getsim -d /scratch/myname/simulations mycluster bbh_1_* bns_3_{24,28}

## Tutorial

Clone the repository somewhere in your home directory:

    cd $HOME
    git clone https://ianhinder@bitbucket.org/ianhinder/getsim.git

Change into your work directory.  This might be your Cactus directory,
or a directory associated with your project.

    cd $HOME/Cactus/MyProject

Copy the filter rules file into your project directory;

    cp $HOME/getsim/getsim.filter.rules .

Configure the destination directory for simulations on your local
machine:

    echo "$HOME/simulations/MyProject" >getsim.destdir

Suppose you have a simulation on a remote cluster.  Configure the
machine in your .ssh/config so that you can ssh to it using 'ssh
<machinename>'.

Run getsim to transfer a Cactus simulation from the remote machine:

    getsim -d /scratch/myname/simulations mycluster bbh_1_24

The simulation will appear in $HOME/simulations/MyProject.

## Status

The author has used getsim for many years, but it is still very
simplistic.

